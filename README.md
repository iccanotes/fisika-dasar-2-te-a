# Fisika Dasar 2 TE-A


## Learning Outcome Outline					
## Pertemuan



Persamaan | Learning Outcome | Materi & Asesmen
---|---|---
1-2 | <ol><li>Mampu Mengenali konsep Osilasi </li><li> Memahami konsep Osilasi Harmonis </li><li>Mampu memahami perhitungan Persamaan umum Gelombang</li><li>Mampu Memahami aplikasi osilasi harmonis pada beberapa bentuk|<ol><li>[Lectures note/Catatan]() 
3-4 | <ol><li>Mengenal konsep Medan Listrik</li><li> Mampu memahami perhitungan medan magnet dan resultan untuk beberapa muatan</li><li> Memahami tentang dua dipol </li><li> Memahami tentang distribusi medan listrik dengan muatan terdistribusi pada bidang terentu |<ol><li> [Pendahulua Medan Listrik](https://youtu.be/r6oIRItxUYU?si=qpFxDRkvWzdxn4BI) </li><li> [Hubungan Medan Listrik dan Gaya Listrik](https://youtu.be/S8y6_ZNlqtU?si=T2wfTvOY7e6p8Klg) </li><li> [Latihan Soal 1 Medan Listrik dua Muatan](https://youtu.be/HHep6Ko1Deg?si=r8oeFnswoNsp7UZF)</li><li> [Latihan Soal Analasis Vektor Medan Listrik dua Muatan](https://youtu.be/tHbBSseBogY?si=Nwc1o6uoR0_QxqXO) </li><li> [Medan Listrik Dua dipol (analasis Vektor)](https://youtu.be/e_gDxLIub9w?si=ig5_vrlRbDQLBK-k) </li><li> [Medan Listrik Dua dipol Lanjutan](https://youtu.be/50q7L5WDZ3k?si=U4svLVr9-wi4BgkZ) </li><li> [Medan Listrik dan Muatan Kontinu](https://youtu.be/7-uCPxYKtEE?si=ShxBcQqBmzxdGfB0) </li><li> [Medan Listrik disebabkan oleh batang bermuatan](https://youtu.be/2C-cRXE6O_g?si=sczt4_y0W71Lqtln) </li><li> [Tugas Medan Listrik 1](https://drive.google.com/file/d/11cF2VyHWgcBbUthjMsezRvYkh6iAIVsJ/view?usp=share_link) 
5-6 | <ol><li>Mampu Memahami tentang hukum gaus </li><li> mampu menghitung medan magnt dengan banyak kaksus menggunakan hukum gauss |<ol><li>[Lectures note/Catatan](https://youtu.be/NDKV2gg6j1Y?si=GuS4dYcdf4PvUvwm) 
7 | <ol><li>Mampu Memahami tentang Potensial Listrik </li><li> mampu memaami tentang konsep energi dan potensial listrik |<ol><li>[Lectures note/Catatan](https://drive.google.com/file/d/1fdhJg6fTflPxKxJ-6vHlcXIGt0ImassQ/view?usp=share_link) 
8-9 | <ol><li>Mampu Memahami tentang Kapasitor </li><li> Mengenali material kapasitor </li><li> Merangkai dan menghitungan rangakaian di kapasitor|<ol><li>[Lectures note/Catatan](https://drive.google.com/file/d/1fe6Tb_X3pvyw0Joavz4aIlR9qyXtpNQa/view?usp=share_link) 
10 | Review Materi dan Diskusi Research By Learning (Project)
11 | <ol><li>Mampu memahami konsep Listrik Dimanis </li><li> mampu memahami tentang konsep kecepatan aliran listrik </li><li> mampu memahami tentang konsep Resistansi </li><li> mampu memahami tentang konsep Resistor|<ol><li>[Intro : Listrik Dinamis](https://youtu.be/O33Tgy_vdrk?si=IkaYxwD0h3Hrycik) </li><li> [Kecepatan Aliran Listrik](https://youtu.be/dEKHxhur4gM?si=GS3Tz-8akA_Qxj_S) </li><li> [Konsep Resistansi](https://youtu.be/Ml7j2S00c5A?si=WyycbVAu9Ug-6y1m) </li><li> [Resistor](https://youtu.be/wf9H49lsFbs?si=fGaxwdtDH8IJ__kl)
12 | <ol><li>Mampu memahami konsep Hukum Ohm </li><li> mampu memahami tentang konsep Rangkaian seri dan pararel pada rangkaian listrik </li><li> mampu menghitung menggunakan hukum kirchof I </li><li> mampu memahami menggunakan hukum kirchof II |<ol><li>[Hukum Ohm](https://youtu.be/CslXd3gvs0k?si=e0Fu-TuQWjQAEDMG) </li><li> [GGL dan Hambatan dalam](https://youtu.be/2QtI3Zj7ws4?si=3-J4mbwpqgdOI0Ra)</li><li> [Hukum Kirchof I](https://youtu.be/Uhz18OHyMng?si=eVXMYDGoLTJO6lXP) </li><li> [Hukum Kirchof II](https://youtu.be/SUKluqvhaSU?si=1gnP2B2bb0BrUKlu) </li><li> [Lanjutan Hukum Kirchof II](https://youtu.be/NDKV2gg6j1Y?si=GuS4dYcdf4PvUvwm)


